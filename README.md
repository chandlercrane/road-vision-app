![Swift version](https://img.shields.io/static/v1.svg?label=Swift&message=4.2&color=orange)
![Platform support](https://img.shields.io/static/v1.svg?label=iOS&message=%3E=%2011.2&color=brightgreen)

# Road Vision App

This repository is for a stand-alone app that uses the [Mapbox Vision SDK](https://vision.mapbox.com/) to grab road data and send it to DataFlow, AT&T's IoT platform.

This app sends data to the DataFlow project [Road Vision App](https://portal.dataflow.iot.att.com/projects/tdp/road-vision-app/)

## Tokens
In order to fetch and use Vision SDK, you will need to obtain two tokens at [tokens page in your Mapbox account](https://account.mapbox.com/access-tokens/create/):
1. **Public token:** create a token that includes the `vision:read` scope
1. **Private token:** create another token with the `vision:download` scope

## Installation
1. `git clone https://gitlab.com/chandlercrane/road-vision-app.git`
1. `cd road-vision-app`
1. Open `Podfile` (e.g. `vim Podfile`)
1. Put your *private* token instead of `<ADD-PRIVATE-TOKEN-HERE>` in `Podfile`
1. Close `Podfile` (e.g. `esc :wq` in  `Podfile`)
1. Put your *public* token into the value of the `MGLMapboxAccessToken` key within the `Info.plist` file
1. Run `pod install`
1. Run the application

## Contributions
1. [Chandler Crane](http://linkedin.com/in/chandlercrane)
1. **Srinivas Nukala** (*AT&T*)
1. **Tory Smith** (*Mapbox*)

## App Images
<img src="images/app-login.png"  width="50%" height="50%">
<img src="images/app-vision.png"  width="50%" height="50%">
<img src="images/app-settings.png"  width="50%" height="50%">
<img src="images/app-bluetooth.png"  width="50%" height="50%">


