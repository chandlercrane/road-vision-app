//
//  VisionContainerViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/25/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- VisionContainerViewController.swift -- Chandler Crane -- 8/8/19
 
 This file was modified from the ExampleContainerViewController.swift file from a Vision iOS example app, though it was heavily modified. It now contains logic for the main view of the app. In addition to this critical part of the app, it also contains the global variable declaration of the Calamp variable. This is defined from the Device struct also shown below. The Calamp object is what the app will search for as an acceptable Calamp device to connect to on start-up. It does so by seeing whether the name, uuid, or stat are contained within the name of the respective name, uuid, or stat found in a bluetooth object. It is also worth noting that though the app does search for bluetooth devices, it does not contain any logic to connect to a device -- that is not within the scope of the project. What it does do, is remember the name, uuid, and description which it sends to DF in the API calls so that Calamp data can be paired with app data on the back-end. This has not been configured yet, but I think it is fairly trivial given speed data/location data/etc.
 */


import Foundation
import UIKit
import CoreMedia
import MapboxNavigation
import MapboxVision
import MapboxVisionAR
import CoreBluetooth

var calampName = ""
var calampUUID = ""
var calampStat = ""
struct Device{
    var name = ""
    var uuid = ""
    var stat = ""
}

// Change this based on what you qualify as a Calamp device to connect with
// This is set up where the app checks if the bluetooth device name contains the name set below
// AND whether the bluetooth device uuid contains the uuid set below!
var Calamp = Device(name: "CalAmp", uuid: "", stat: "")

private let bannerInset: CGFloat = 18
private let roadLanesHeight: CGFloat = 64
private let smallRelativeInset: CGFloat = 16
private let buttonHeight: CGFloat = 36

final class VisionContainerViewController: UIViewController {
    
    weak var delegate: ContainerDelegate? {
        willSet {
            settingsButton.removeTarget(delegate, action: #selector(ContainerDelegate.settingsButtonPressed), for: .touchUpInside)
            bluetoothButton.removeTarget(delegate, action: #selector(ContainerDelegate.bluetoothButtonPressed), for: .touchUpInside)
        }
        didSet {
            settingsButton.addTarget(delegate, action: #selector(ContainerDelegate.settingsButtonPressed), for: .touchUpInside)
            bluetoothButton.addTarget(delegate, action: #selector(ContainerDelegate.bluetoothButtonPressed), for: .touchUpInside)
        }
    }
    
    var visionViewController: VisionPresentationViewController?
    
    // bluetooth variables
    var centralManager: CBCentralManager?
    private var peripherals = Set<DisplayPeripheral>()
    private var uuids = Set<String>()
    private var selectedPeripheral: CBPeripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        view.addSubview(settingsButton)
        NSLayoutConstraint.activate([
            settingsButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            settingsButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60),
            settingsButton.widthAnchor.constraint(lessThanOrEqualToConstant: 44),
            settingsButton.heightAnchor.constraint(lessThanOrEqualToConstant: 44)
            ])
        
        /*
        view.addSubview(bluetoothButton)
        NSLayoutConstraint.activate([
            bluetoothButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            bluetoothButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 60),
            bluetoothButton.widthAnchor.constraint(lessThanOrEqualToConstant: 44),
            bluetoothButton.heightAnchor.constraint(lessThanOrEqualToConstant: 44)
            ])
        */
        
        view.addSubview(roadLanesView)
        NSLayoutConstraint.activate([
            roadLanesView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            roadLanesView.topAnchor.constraint(equalTo: view.topAnchor, constant: bannerInset),
            roadLanesView.heightAnchor.constraint(equalToConstant: roadLanesHeight),
            ])
        
        view.addSubview(signsStack)
        NSLayoutConstraint.activate([
            signsStack.topAnchor.constraint(equalTo: view.topAnchor),
            signsStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ])
        
        //Initialize CoreBluetooth Central Manager
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // bluetooth functionality
        selectedPeripheral = nil
    }
    
    override func present(viewController: UIViewController) {
        super.present(viewController: viewController)
        view.insertSubview(viewController.view, belowSubview: settingsButton)        
    }
    
    
    private let settingsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Asset.Assets.settings.image, for: .normal)
        return button
    }()
    
    
    private let bluetoothButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Asset.Assets.settings.image, for: .normal)
        return button
    }()
    
    
    private let roadLanesView: RoadLanesView = {
        let view = RoadLanesView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    private let signsStack: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.spacing = smallRelativeInset
        view.alignment = .center
        view.axis = .horizontal
        view.isHidden = true
        return view
    }()
    
    private let speedLimitView: ImageSwitchingView = {
        let view = ImageSwitchingView()
        view.spacing = bannerInset
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var currentViewController: UIViewController?
    
}

extension VisionContainerViewController: ContainerPresenter {
    
    func present(frame: CMSampleBuffer) {
        visionViewController?.present(sampleBuffer: frame)
    }
    
    func present(detections: FrameDetections) {
        visionViewController?.present(detections: detections)
    }
    
    func present(signs: [ImageAsset]) {
        signsStack.subviews.forEach { $0.removeFromSuperview() }
        signsStack.isHidden = signs.isEmpty
        signs.map { UIImageView(image: $0.image) }.forEach(signsStack.addArrangedSubview)
    }
    
    func present(roadDescription: RoadDescription?) {
        DispatchQueue.main.async {
            guard let roadDescription = roadDescription else {
                self.roadLanesView.isHidden = true
                return
            }
            self.roadLanesView.isHidden = true
            self.roadLanesView.update(roadDescription)
        }
    }
    
    func present(speedLimit: ImageAsset?, isNew: Bool) {
        guard let speedLimit = speedLimit else {
            speedLimitView.reset()
            speedLimitView.isHidden = true
            return
        }
        
        speedLimitView.isHidden = false
        if isNew {
            speedLimitView.switch(to: speedLimit.image)
        } else {
            speedLimitView.image = speedLimit.image
        }
    }
    
    func present(screen: Screen) {
        switch screen {
        case .signsDetection: visionViewController?.frameVisualizationMode = .clear
        }
    }
    
    func presentVision() {
        guard let viewController = visionViewController else {
            assertionFailure("Vision should be initialized before presenting")
            return
        }
        
        present(viewController: viewController)
        viewController.frameVisualizationMode = .clear
    }
    
    func settingsButtonClicked(){
         guard let controller = UIStoryboard(name: "Main", bundle: .main)
         .instantiateViewController(withIdentifier: "settingsView") as? SettingsViewController else {
         assertionFailure("Unknown controller type")
         return
         }
        
         present(controller, animated: true)
    }
    
    
    func bluetoothButtonClicked(){
        guard let controller = UIStoryboard(name: "Main", bundle: .main)
            .instantiateViewController(withIdentifier: "bluetoothNavView") as? UINavigationController else {
                assertionFailure("Unknown controller type")
                return
        }
        //present(controller, animated: true)
        //performSegue(withIdentifier: "popoverBluetoothSegue", sender: self)
    }
    
    
    func presentSettingsButton(isVisible: Bool) {
        settingsButton.isHidden = !isVisible
    }
    
    func presentBluetoothButton(isVisible: Bool) {
        bluetoothButton.isHidden = !isVisible
    }
    
    func dismissCurrent() {
        guard let viewController = currentViewController else { return }
        dismiss(viewController: viewController)
        currentViewController = nil
    }
}

extension UIViewController {
    
    @objc func present(viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        NSLayoutConstraint.activate([
            viewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            viewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            viewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            viewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
        viewController.didMove(toParent: self)
    }
    
    func dismiss(viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}


extension VisionContainerViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        if (central.state == .poweredOn){
            //scanningButton.isEnabled = true
            startScanning()
        }else{
            //scanningButton.isEnabled = false
            peripherals.removeAll()
            uuids.removeAll()
        }
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
        if(uuids.contains(peripheral.identifier.uuidString) || peripheral.name == nil){
            // here we ignore the new peripheral since it's already been marked or is nil
        }
        else{
            // peripheral has not been seen yet, add its uuid to set and add to table
            uuids.insert(peripheral.identifier.uuidString)
            peripherals.insert(displayPeripheral)
            if calampName == "" {
                checkIfCalamp(p: peripheral)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("Service: ", service)
            print("Service UUID: ", service.uuid)
        }
    }
    
    
    @IBAction private func scanningButtonPressed(_ sender: AnyObject){
        if centralManager!.isScanning{
            centralManager?.stopScan()
        }else{
            startScanning()
        }
    }
    
    private func startScanning() {
        peripherals = []
        self.centralManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.centralManager!.isScanning {
                strongSelf.centralManager?.stopScan()
            }
        }
    }
    func checkIfCalamp(p: CBPeripheral){
        // Checking if its name contains "Calamp" to see if there is a calamp...
        if p.name!.contains(Calamp.name) || p.identifier.uuidString.contains(Calamp.uuid) {
            calampName = p.name!
            calampUUID = p.identifier.uuidString
        }
    }
}
