//
//  AppDelegate.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/15/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- AppDelegate.swift -- Chandler Crane -- 8/8/19
 
 This file provides the starting logic for the app, whether or not the first window will be the login screen or the vision VC. It allows has some code for the processing of the login through its delegate.
 */


import MapboxVision
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var interactor: ContainerInteractor?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        _ = MTLCreateSystemDefaultDevice()
        
        application.isIdleTimerDisabled = true
                
        window!.rootViewController = makeRootViewController()
        window!.makeKeyAndVisible()
        
        return true
    }
    
    private func makeRootViewController() -> UIViewController {
        if LoginController.checkSubmission() {
            return launchVision()
        } else {
            return launchLogin()
        }
    }
    
    private func launchVision() -> UIViewController {
        let containerController = VisionContainerViewController()
        
        containerController.visionViewController = VisionPresentationViewController()
                
        interactor = ContainerInteractor(dependencies: ContainerInteractor.Dependencies(
            presenter: containerController
        ))
        containerController.delegate = interactor
        
        return containerController
    }
    
    private func launchLogin() -> UIViewController {
        let viewController = LoginViewController()
        viewController.loginDelegate = self
        return viewController
    }

}

extension AppDelegate: LoginDelegate {
    func loginSubmitted(username: String, password: String) {
        LoginController.submit(username: username, password: password)
        window!.rootViewController = launchVision()
    }
}
