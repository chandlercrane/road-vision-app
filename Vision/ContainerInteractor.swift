//
//  ContainerInteractor.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/31/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- ContainerInteractor.swift -- Chandler Crane -- 8/8/19
 
 This file was taken from the Mapbox Vision Teaser App and broken down to handle delegation for only sign detection functionality. It can be seen that from the original, several pieces were removed to accommodate the singular view. There were also some functions added to allow for button press delegates to be created. 
 */


import Foundation
import MapboxVision
import MapboxVisionAR
import MapboxVisionSafety
import CoreMedia
import CoreLocation
import UIKit

enum Screen {
    case signsDetection
}

protocol ContainerPresenter: class {
    func presentVision()
    func settingsButtonClicked()
    func bluetoothButtonClicked()
    func present(screen: Screen)
    func presentSettingsButton(isVisible: Bool)
    func presentBluetoothButton(isVisible: Bool)
    func present(roadDescription: RoadDescription?)
    func present(frame: CMSampleBuffer)
    func present(detections: FrameDetections)
    func present(signs: [ImageAsset])
    func present(speedLimit: ImageAsset?, isNew: Bool)
    func dismissCurrent()
}

@objc protocol ContainerDelegate: AnyObject {
    func settingsButtonPressed()
    func bluetoothButtonPressed()
}

private let signTrackerMaxCapacity = 3
final class ContainerInteractor {
    
    private var oAuthToken = ""
    private struct SpeedLimitState: Equatable {
        let speedLimits: SpeedLimits
        let isSpeeding: Bool
    }
    
    private class AutoResetRestriction {
        var isAllowed: Bool = true
        
        private let resetInterval: TimeInterval
        
        private var timer: Timer?
        
        init(resetInterval: TimeInterval) {
            self.resetInterval = resetInterval
        }
        
        func restrict() {
            isAllowed = false
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: resetInterval, repeats: false) { [weak self] _ in
                self?.isAllowed = true
            }
        }
    }
    
    private var currentScreen = Screen.signsDetection
    private let presenter: ContainerPresenter
    private let visionManager: VisionManagerProtocol
    
    private var visionARManager: VisionARManager?
    private var visionSafetyManager: VisionSafetyManager?
    private let camera: VideoSource
    
    private lazy var delegateProxy = DelegateProxy(delegate: self)
    
    private let signTracker = Tracker<Sign>(maxCapacity: signTrackerMaxCapacity)
    private var signTrackerUpdateTimer: Timer?
    
    private var lastSpeedLimitState: SpeedLimitState?

    /*------------------------------------
     ----------DATA FLOW INSTANCE----------
     ------------------------------------*/
    private var dataFlow = DataFlow()
    
    //vision values caching
    private var speedLimits: SpeedLimits?
    private var currentSpeed: Float?
    private var currentCountry: Country = .unknown
    
    private var currentSpeedLimit: Float?
    
    struct Dependencies {
        let presenter: ContainerPresenter
    }
    
    init(dependencies: Dependencies) {
        self.presenter = dependencies.presenter
        
        let camera = CameraVideoSource()
        camera.start()
        let visionManager = VisionManager.create(videoSource: camera)
        
        self.camera = camera
        self.visionManager = visionManager
        
        visionSafetyManager = VisionSafetyManager.create(visionManager: visionManager, delegate: delegateProxy)
        
        camera.add(observer: self)
        visionManager.start(delegate: delegateProxy)
        
        presenter.presentVision()
        present(screen: .signsDetection)
        scheduleSignTrackerUpdates()
    }
    
    private func scheduleSignTrackerUpdates() {
        stopSignTrackerUpdates()
        
        signTrackerUpdateTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            guard let `self` = self else { return }
            let signs = self.signTracker.getCurrent().compactMap { self.getIcon(for: $0, over: false) }
            
            // START DATA FLOW ADDITION
            
            var locManager = CLLocationManager()
            //locManager.requestWhenInUseAuthorization()
            var currentLocation: CLLocation!
            
            if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways){
                currentLocation = locManager.location
            }
            let speedLimit : Int = Int(self.speedLimits?.speedLimitRange.max ?? 0 * 2.237)
            
            var isSchoolZone : Bool = false
            for sign in signs{
                if(sign.name == "Warning_School_Zone_US"){
                    isSchoolZone = true
                }
            }
            for sign in signs{
                if(sign.name == "Regulatory_End_Of_School_Zone_US"){
                    isSchoolZone = false
                }
            }
            
            if (signs.count > 0){
                /*-----------------------
                 -----DATA FLOW SEND------
                 -----------------------*/
                for sign in signs{
                    self.dataFlow.sendAllData(signName: sign.name, isSchoolZone: isSchoolZone, longitude: currentLocation.coordinate.longitude, latitude: currentLocation.coordinate.latitude, speedMetric: currentLocation.speed)
                    
                }
            }
            
            // END DATA FLOW ADDITION
            
            self.presenter.present(signs: signs)
        }
    }
    
    private func stopSignTrackerUpdates() {
        signTrackerUpdateTimer?.invalidate()
        signTracker.reset()
    }
    
    private func modelPerformanceConfig(for screen: Screen) -> ModelPerformanceConfig {
        switch screen {
        case .signsDetection:
            return .merged(performance: ModelPerformance(mode: .fixed, rate: .high))
        }
    }
    
    private func resetPresentation() {
        stopSignTrackerUpdates()
        presenter.present(signs: [])
    }
    
    private func present(screen: Screen) {
        presenter.dismissCurrent()
        visionManager.modelPerformanceConfig = modelPerformanceConfig(for: screen)
        presenter.present(screen: screen)
        presenter.presentSettingsButton(isVisible: true)
        //presenter.presentBluetoothButton(isVisible: true)
        currentScreen = screen
    }
    
    /* START DATA FLOW ADDITION
    // changed POSITION to CLLOCATION
    private func check(_ speedLimit: SpeedLimits, at position: CLLocation?) -> Bool {
        guard let position = position else { return false }
        return position.speed > Double(speedLimit.speedLimitRange.max)
    }
    
    // changed POSITION to CLLOCATION
    private func update(speedLimit: SpeedLimits?, position: CLLocation?) {
        guard
            let speedLimits = speedLimits
            else {
                presenter.present(speedLimit: nil, isNew: false)
                lastSpeedLimitState = nil
                return
        }
        
        let isSpeeding = check(speedLimits, at: position)
        let newState = SpeedLimitState(speedLimits: speedLimits, isSpeeding: isSpeeding)
        
        guard newState != lastSpeedLimitState else { return }
        
        NSLog("Position speed is - %0.4f, speedlimit is %0.4f", position?.speed ?? -1 , Double(speedLimits.speedLimitRange.max))
        /*-----------------------
         -----DATA FLOW SEND------
         -----------------------*/
        
        print("======ABOUT TO SEND DATA...=======")
        
        dataFlow.sendData(speedLimit: speedLimit, position: position)
        
        presentSpeedLimit(oldState: lastSpeedLimitState, newState: newState)
        playSpeedLimitAlert(oldState: lastSpeedLimitState, newState: newState)
        
        lastSpeedLimitState = newState
    }
    */
    
    // END DATA FLOW ADDITION
    
    private func updateSpeedLimits() {
        guard
            let speedLimits = speedLimits,
            let speed = currentSpeed
            else {
                presenter.present(speedLimit: nil, isNew: false)
                lastSpeedLimitState = nil
                return
        }
        
        let isSpeeding = speed > speedLimits.speedLimitRange.max
        let newState = SpeedLimitState(speedLimits: speedLimits, isSpeeding: isSpeeding)
        
        guard newState != lastSpeedLimitState else { return }
        NSLog("Position speed is - %0.4f, speedlimit is %0.4f", speed, Double(speedLimits.speedLimitRange.max))
        //update(speedLimit: speedLimits, position: nil)
        
        presentSpeedLimit(oldState: lastSpeedLimitState, newState: newState)
        playSpeedLimitAlert(oldState: lastSpeedLimitState, newState: newState)
        
        lastSpeedLimitState = newState
    }
    
    private func presentSpeedLimit(oldState: SpeedLimitState?, newState: SpeedLimitState) {
        print("present speed limit function")
        let sign = getIcon(for: Sign(type: .speedLimit, number: newState.speedLimits.speedLimitRange.max), over: newState.isSpeeding)
        let isNew = oldState == nil || oldState!.speedLimits != newState.speedLimits
        presenter.present(speedLimit: sign, isNew: isNew)
        //print("current speed: ", self.currentSpeed)
        //print("speed limit: ", newState.speedLimits.speedLimitRange.max)
    }
    
    private func playSpeedLimitAlert(oldState: SpeedLimitState?, newState: SpeedLimitState) {
        let wasSpeeding = oldState?.isSpeeding ?? false
        let hasStartedSpeeding = newState.isSpeeding && !wasSpeeding
    }
    
    private func getIcon(for sign: Sign, over: Bool) -> ImageAsset? {
        return sign.icon(over: over, country: currentCountry)
    }
    
    deinit {
        camera.remove(observer: self)
    }
}

extension ContainerInteractor: ContainerDelegate {
    func settingsButtonPressed() {
        resetPresentation()
        presenter.settingsButtonClicked()
    }
    
    func bluetoothButtonPressed() {
        presenter.bluetoothButtonClicked()
    }
 
}

extension ContainerInteractor: VisionManagerDelegate {
    
    func visionManager(_ visionManager: VisionManagerProtocol, didUpdateFrameSignClassifications frameSignClassifications: FrameSignClassifications) {
        guard case .signsDetection = currentScreen else { return }
        let items = frameSignClassifications.signs.map({ $0.sign })
        signTracker.update(items)
        /*
         print("------start frame sign classification------")
         for item in items{
         print(item.type)
         }
         print("-------end frame sign classifcation-------")
         */
    }
    
    func visionManager(_ visionManager: VisionManagerProtocol, didUpdateVehicleState vehicleState: VehicleState) {
        currentSpeed = vehicleState.speed
    }
    
    func visionManager(_ visionManager: VisionManagerProtocol, didUpdateRoadDescription roadDescription: RoadDescription) {
        presenter.present(roadDescription: roadDescription)
    }
    
    func visionManager(_ visionManager: VisionManagerProtocol, didUpdateCountry country: Country) {
        currentCountry = country
    }
    
    func visionManagerDidCompleteUpdate(_ visionManager: VisionManagerProtocol) {
        updateSpeedLimits()
    }
    // ADDED FROM SRINIVAS BUT DOES NOT WORK CURRENTLY
    
    func visionManager(_ visionManager: VisionManager, didUpdateEstimatedPosition estimatedPosition: CLLocation?) {
        // NSLog("In updated estimated position %0.6f, %0.6f", estimatedPosition?.clLocation.coordinate.latitude ?? 0,estimatedPosition?.clLocation.coordinate.longitude ?? 0)
        
        // print(estimatedPosition?.coordinate.longitude ??  0,estimatedPosition?.coordinate.latitude ?? 0)
        print("updated estimated position")
    }
    
}

extension ContainerInteractor: VideoSourceObserver {
    func videoSource(_ videoSource: VideoSource, didOutput videoSample: VideoSample) {
        DispatchQueue.main.async { [weak self] in
            self?.presenter.present(frame: videoSample.buffer)
        }
    }
}


extension ContainerInteractor: VisionSafetyManagerDelegate {
    func visionSafetyManager(_ visionSafetyManager: VisionSafetyManager, didUpdateRoadRestrictions roadRestrictions: RoadRestrictions) {
        speedLimits = roadRestrictions.speedLimits
        print("this is executing...")
        let locManager = CLLocationManager()
        locManager.distanceFilter = kCLDistanceFilterNone
        locManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        let currentLocation = locManager.location
        
        //let currentElevation =
        //let speedLimit : Int = Int(speedLimit_metric * 2.237)
        
        let speedLimit_metric : Float = speedLimits!.speedLimitRange.max
        
        let currentSpeed : Float = Float(currentLocation?.speed ?? 0)
        let altitude = currentLocation?.altitude
        let latitude = currentLocation?.coordinate.latitude ?? 0
        let longitude = currentLocation?.coordinate.longitude ?? 0
        
        print("speed limit (m/s): ", speedLimit_metric)
        print("location: ", currentLocation)
        print("speed: ", currentSpeed)
        print("latitude: ", latitude)
        print("longitude: ", longitude)
        
        self.currentSpeedLimit = speedLimit_metric
        self.dataFlow.sendSpeedData(currentSpeed: currentSpeed, speedLimit: speedLimit_metric, longitude: longitude, latitude: latitude)
    }
}


