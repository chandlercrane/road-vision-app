//
// Created by Alexander Pristavko on 2019-03-29.
// Copyright (c) 2019 Mapbox. All rights reserved.
//

import Foundation
import UIKit
import MapboxVision
import MapboxVisionSafety
import CoreBluetooth

/**
 * "Over speeding" example demonstrates how to utilize events from MapboxVisionSafetyManager to alert a user about exceeding allowed speed limit.
 */

class OverSpeedingViewController: UIViewController {
    private var videoSource: CameraVideoSource!
    private var visionManager: VisionManager!
    private var visionSafetyManager: VisionSafetyManager!
    
    private let visionViewController = VisionPresentationViewController()
    private var alertView: UIView!
    
    private var vehicleState: VehicleState?
    private var restrictions: RoadRestrictions?
    
    // bluetooth variables
    var centralManager: CBCentralManager?
    private var peripherals = Set<DisplayPeripheral>()
    private var uuids = Set<String>()
    private var selectedPeripheral: CBPeripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addVisionView()
        addAlertView()
        
        // create a video source obtaining buffers from camera module
        videoSource = CameraVideoSource()
        videoSource.add(observer: self)
        
        // create VisionManager with video source
        visionManager = VisionManager.create(videoSource: videoSource)
        // create VisionSafetyManager and register as its delegate to receive safety related events
        visionSafetyManager = VisionSafetyManager.create(visionManager: visionManager, delegate: self)
        
        // check bluetooth to get current calamp...
        //Initialize CoreBluetooth Central Manager
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        visionManager.start(delegate: self)
        videoSource.start()
        
        // bluetooth functionality
        selectedPeripheral = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        videoSource.stop()
        visionManager.stop()
        // free up resources by destroying modules when they're not longer used
        //visionSafetyManager.destroy()
    }
    
    private func addVisionView() {
        addChild(visionViewController)
        view.addSubview(visionViewController.view)
        visionViewController.didMove(toParent: self)
    }
    
    private func addAlertView() {
        alertView = UIImageView(image: UIImage(named: "alert"))
        alertView.isHidden = true
        alertView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(alertView)
        NSLayoutConstraint.activate([
            alertView.topAnchor.constraint(equalToSystemSpacingBelow: view.topAnchor, multiplier: 1),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: alertView.trailingAnchor, multiplier: 1),
            ])
    }
}

extension OverSpeedingViewController: VisionManagerDelegate, VisionSafetyManagerDelegate {
    func visionManager(_ visionManager: VisionManagerProtocol, didUpdateVehicleState vehicleState: VehicleState) {
        DispatchQueue.main.async { [weak self] in
            // save the latest state of the vehicle
            self?.vehicleState = vehicleState
            print("latitude: " + String(vehicleState.location.coordinate.lat))
            print("longitude: " + String(vehicleState.location.coordinate.lon))
            print("speed: " + String(vehicleState.speed))
        }
    }
    
    func visionSafetyManager(_ visionSafetyManager: VisionSafetyManager, didUpdateRoadRestrictions roadRestrictions: RoadRestrictions) {
        DispatchQueue.main.async { [weak self] in
            // save currenly applied road restrictions
            self?.restrictions = roadRestrictions
            print("speed limit: " + String(roadRestrictions.speedLimits.speedLimitRange.max))
        }
    }
    
    func visionManagerDidCompleteUpdate(_ visionManager: VisionManagerProtocol) {
        DispatchQueue.main.async { [weak self] in
            // when update is completed all the data has the most current state
            guard let state = self?.vehicleState, let restrictions = self?.restrictions else { return }
            
            // decide whether speed limit is exceeded by comparing it with the current speed
            let isOverSpeeding = state.speed > restrictions.speedLimits.speedLimitRange.max
            self?.alertView.isHidden = !isOverSpeeding
            print("another speed reading: " + String(state.speed))
            print("another speed limit reading: " + String(restrictions.speedLimits.speedLimitRange.max))
            print("speeding? " + String(isOverSpeeding))
        }
    }
}

extension OverSpeedingViewController: VideoSourceObserver {
    public func videoSource(_ videoSource: VideoSource, didOutput videoSample: VideoSample) {
        DispatchQueue.main.async { [weak self] in
            // display received sample buffer by passing it to presentation controller
            self?.visionViewController.present(sampleBuffer: videoSample.buffer)
        }
    }
}

extension OverSpeedingViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        if (central.state == .poweredOn){
            //scanningButton.isEnabled = true
            startScanning()
        }else{
            //scanningButton.isEnabled = false
            peripherals.removeAll()
            uuids.removeAll()
        }
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
        if(uuids.contains(peripheral.identifier.uuidString) || peripheral.name == nil){
            // here we ignore the new peripheral since it's already been marked or is nil
        }
        else{
            // peripheral has not been seen yet, add its uuid to set and add to table
            uuids.insert(peripheral.identifier.uuidString)
            peripherals.insert(displayPeripheral)
            if calampName == "" {
                checkIfCalamp(p: peripheral)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("Service: ", service)
            print("Service UUID: ", service.uuid)
        }
    }
    
    
    @IBAction private func scanningButtonPressed(_ sender: AnyObject){
        if centralManager!.isScanning{
            centralManager?.stopScan()
        }else{
            startScanning()
        }
    }
    
    private func startScanning() {
        peripherals = []
        self.centralManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.centralManager!.isScanning {
                strongSelf.centralManager?.stopScan()
            }
        }
    }
    func checkIfCalamp(p: CBPeripheral){
        // Checking if its name contains "Calamp" to see if there is a calamp...
        if p.name!.contains(Calamp.name) || p.identifier.uuidString.contains(Calamp.uuid) {
            calampName = p.name!
            calampUUID = p.identifier.uuidString
        }
    }
}
