//
//  CalampViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/29/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- CalampViewController.swift -- Chandler Crane -- 8/8/19

 This file is a table view that lists the bluetooth devices found in the search. If the device is currently the selected calamp device, it shows up light green in the table. Nothing too out of the ordinary in this file.
 */


import Foundation
import UIKit
import CoreBluetooth

struct DisplayPeripheral: Hashable {
    let peripheral: CBPeripheral
    let lastRSSI: NSNumber
    let isConnectable: Bool
    
    var hashValue: Int { return peripheral.hashValue }
    
    static func ==(lhs: DisplayPeripheral, rhs: DisplayPeripheral) -> Bool {
        return lhs.peripheral == rhs.peripheral
    }
}

protocol DeviceCellDelegate: class {
    func didTapConnect(_ cell: CalampTableCell, peripheral: CBPeripheral)
}

class CalampTableCell: UITableViewCell{
    @IBOutlet weak var bluetoothDeviceName: UILabel!
    private var displayPeripheral: DisplayPeripheral!
    weak var delegate: DeviceCellDelegate?
    var minHeight: CGFloat?

    func populate(displayPeripheral: DisplayPeripheral) {
        self.displayPeripheral = displayPeripheral
        bluetoothDeviceName.text = displayPeripheral.peripheral.name
        minHeight = 60
        let softgreen = UIColor(red: 117/255.0, green: 216/255.0, blue: 146/255.0, alpha: 1)
        let softgrey = UIColor(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1)
        let newName = displayPeripheral.peripheral.name
        let newUUID = displayPeripheral.peripheral.identifier.uuidString
        let newStat = displayPeripheral.peripheral.identifier.description
        if(newName == calampName && newUUID == calampUUID){
            print("calamp match at name: ", calampName)
            print("with name: ", newName!)
            print("calamp match at uuid: ", calampUUID)
            print("with uuid: ", newUUID)
            print("calamp match at description: ", calampStat)
            print("with description: ", newStat)
            backgroundColor = softgreen
        }
        else{
            backgroundColor = softgrey
        }
        //deviceRssiLabel.text = "\(displayPeripheral.lastRSSI)dB"
        //connectButton.isHidden = !displayPeripheral.isConnectable
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        guard let minHeight = minHeight else { return size }
        return CGSize(width: size.width, height: max(size.height, minHeight))
    }
}

final class CalampViewController: UITableViewController{
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBAction func closeCalampDevices(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var deviceName = ""
    var deviceUUID = ""
    
    var centralManager: CBCentralManager?
    private var peripherals = Set<DisplayPeripheral>()
    private var uuids = Set<String>()
    private var viewReloadTimer: Timer?
    private var selectedPeripheral: CBPeripheral?
    var connectingViewController: UIViewController?
    // end added
    
    override func viewDidLoad() {
        self.clearsSelectionOnViewWillAppear = false
        //Initialize CoreBluetooth Central Manager
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    // start new added
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedPeripheral = nil
        viewReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(refreshScanView), userInfo: nil, repeats: true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewReloadTimer?.invalidate()
    }
    // end new added
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if peripherals.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "CalampTableCell")!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalampTableCell", for: indexPath) as! CalampTableCell
        
        let peripheralsArray = Array(peripherals)
        if peripheralsArray.count > indexPath.row {
            cell.populate(displayPeripheral: peripheralsArray[indexPath.row])
        }
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return peripherals.count > 0 ? UITableView.automaticDimension : tableView.frame.size.height
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if peripherals.count > 0 {
            return peripherals.count
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let peripheralsArray = Array(peripherals)
        deviceName = peripheralsArray[indexPath.row].peripheral.name!
        deviceUUID = peripheralsArray[indexPath.row].peripheral.identifier.uuidString
        print("from the selected row delegate...")
        print("deviceName: ", deviceName)
        print("deviceUUID: ", deviceUUID)
        guard let controller = UIStoryboard(name: "Main", bundle: .main)
            .instantiateViewController(withIdentifier: "calampInfoView") as? CalampInfoViewController else {
                assertionFailure("Unknown controller type")
                return
        }
        
        controller.deviceNameText = deviceName
        controller.deviceUUIDText = deviceUUID
        navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension CalampViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        if (central.state == .poweredOn){
            //scanningButton.isEnabled = true
            startScanning()
        }else{
            updateStatusText("Bluetooth Disabled")
            //scanningButton.isEnabled = false
            peripherals.removeAll()
            uuids.removeAll()
            tableView.reloadData()
            //UIAlertController.presentAlert(on: self, title: "Bluetooth Unavailable", message: "Please turn bluetooth on")
        }
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
        if(uuids.contains(peripheral.identifier.uuidString) || peripheral.name == nil){
            // here we ignore the new peripheral since it's already been marked or is nil
        }
        else{
            // peripheral has not been seen yet, add its uuid to set and add to table
            print("=================")
            print("name: ", peripheral.name)
            print("uuid: ", peripheral.identifier.uuidString)
            uuids.insert(peripheral.identifier.uuidString)
            peripherals.insert(displayPeripheral)
            tableView.reloadData()
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("Service: ", service)
            print("Service UUID: ", service.uuid)
        }
    }
    private func updateViewForScanning(){
        updateStatusText("Scanning BLE Devices...")
        //scanningButton.update(isScanning: true)
    }
    
    private func updateViewForStopScanning(){
        let plural = peripherals.count > 1 ? "s" : ""
        updateStatusText("\(peripherals.count) Device\(plural) Found")
        //scanningButton.update(isScanning: false)
    }
    
    @IBAction private func scanningButtonPressed(_ sender: AnyObject){
        if centralManager!.isScanning{
            centralManager?.stopScan()
            updateViewForStopScanning()
        }else{
            startScanning()
        }
    }
    
    private func startScanning() {
        updateViewForScanning()
        peripherals = []
        self.centralManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.centralManager!.isScanning {
                strongSelf.centralManager?.stopScan()
                strongSelf.updateViewForStopScanning()
            }
        }
    }
    
    @objc private func refreshScanView() {
        if peripherals.count > 1 && centralManager!.isScanning{
            tableView.reloadData()
        }
    }
    
    private func showLoading() {
        performSegue(withIdentifier: "LoadingSegue", sender: self)
    }
    
    private func updateStatusText(_ text: String) {
        title = text
    }
}

// START ADDED

extension CalampViewController: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        connectingViewController?.dismiss(animated: true, completion: {
            var errorMessage = "Could not connect"
            if let selectedPeripheralName = self.selectedPeripheral?.name {
                errorMessage += " \(selectedPeripheralName)"
            }
            
            if let error = error {
                print("Error connecting peripheral: \(error.localizedDescription)")
                errorMessage += "\n \(error.localizedDescription)"
            }
            
            //UIAlertController.presentAlert(on: self, title: "Error", message: errorMessage)
        })
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        connectingViewController?.dismiss(animated: true, completion: {
            print("Peripheral connected")
            self.performSegue(withIdentifier: "PeripheralConnectedSegue", sender: self)
            peripheral.discoverServices(nil)
        })
    }
}

extension CalampViewController: DeviceCellDelegate {
    func didTapConnect(_ cell: CalampTableCell, peripheral: CBPeripheral) {
        if peripheral.state != .connected {
            selectedPeripheral = peripheral
            peripheral.delegate = self
            centralManager?.connect(peripheral, options: nil)
            showLoading()
        }
    }
}
