//
//  LoginViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/23/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- LoginViewController.swift -- Chandler Crane -- 8/8/19
 
 This file is the view for the primary login screen. It is fairly self explanatory -- nothing out of the ordinary. However, the "verifyUsername" function just returns true. This means that no matter the username and password combination, the app will always proceed as if it is valid. This can be changed in the future if needed. Also, in the "buttonTapped" function, both the username and password are saved to UserDefaults. This is not needed and should be removed in the future if a username/password login is to be implemented. Instead, their could be some sort of token that allows the user to stay signed in until the token has expired.
 */


import Foundation
import UIKit
import MapboxVision

//private let highlightColor = UIColor(red: 0.0, green: 122.0 / 255.0, blue: 1.0, alpha: 1.0)
private let highlightColor = UIColor(red: 71.0 / 255.0, green: 176.0 / 255.0, blue: 194.0 / 255.0, alpha: 1.0)

protocol VisionViewDelegate: class {
    func loginCompleted()
}

protocol LoginDelegate: AnyObject {
    func loginSubmitted(username: String, password: String)
}

final class LoginViewController: UIViewController {
    weak var loginDelegate: LoginDelegate?
    
    let spinnerController = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearFields()
        let tap = UITapGestureRecognizer(target: self.view, action: Selector("endEditing:"))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        setupLayout()
    }
        
    private func setupLayout() {
        stack.addArrangedSubview(titleLabel)
        stack.addArrangedSubview(bodyTextView)
        stack.addArrangedSubview(usernameField)
        stack.addArrangedSubview(passwordField)
        stack.addArrangedSubview(button)
        
        stack.setCustomSpacing(20, after: titleLabel)
        
        view.addSubview(stack)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: 223),
            button.heightAnchor.constraint(equalToConstant: 44),
            bodyTextView.widthAnchor.constraint(equalToConstant: 600),
            bodyTextView.heightAnchor.constraint(equalToConstant: 30),
            usernameField.widthAnchor.constraint(equalToConstant: 300),
            passwordField.widthAnchor.constraint(equalToConstant: 300),
            stack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            //stack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            stack.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20)
            ])
    }
    
    private let stack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 25
        return stack
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AvenirNext-Bold", size: 50)
        label.textColor = .white
        label.text = "Please Sign In" //L10n.welcomeTitle
        return label
    }()
    
    private let usernameField: UITextField = {
        let field = UITextField()
        field.layer.cornerRadius = 22
        field.font = UIFont(name: "AvenirNext-Bold", size: 30)
        field.backgroundColor = .white
        field.textColor = .black
        field.text = ""
        field.placeholder = "Username"
        field.textAlignment = .center
        return field
    }()
    
    private let passwordField: UITextField = {
        let field = UITextField()
        field.layer.cornerRadius = 22
        field.font = UIFont(name: "AvenirNext-Bold", size: 30)
        field.backgroundColor = .white
        field.textColor = .black
        field.text = ""
        field.placeholder = "Password"
        field.textAlignment = .center
        field.isSecureTextEntry = true
        return field
    }()
    
    private let bodyTextView: UITextView = {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let attributes: [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "AvenirNext-Regular", size: 18)!,
            .foregroundColor: UIColor.white,
            .kern: 0.0,
            .paragraphStyle: paragraph,
        ]
        //let text = NSMutableAttributedString(string: L10n.welcomeBody, attributes: attributes)
        let text = NSMutableAttributedString(string: "Please sign in to your DataFlow account.", attributes: attributes)
        text.setSubstringAsLink(substring: L10n.welcomeBodyLinkSubstring, linkURL: "custom")
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "AvenirNext-DemiBold", size: 18.0)!,
            .foregroundColor: highlightColor
        ]
        let textView = UITextView()
        
        textView.backgroundColor = .clear
        
        textView.attributedText = text
        textView.linkTextAttributes = Dictionary(uniqueKeysWithValues:
            linkAttributes.map { return ($0.key, $0.value) }
        )
        textView.isEditable = false
        textView.isUserInteractionEnabled = true
        
        return textView
    }()
    
    private let button: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Sign In", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(highlightColor, for: .normal)
        button.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 22
        
        return button
    }()
    
    @objc private func buttonTapped() {
        let username:String = usernameField.text!
        let password:String = passwordField.text!
        let verified: Bool = verifyUsername(username: username, password: password)
        startSpinner()
        if(verified){
            DispatchQueue.global(qos: .default).async {
                self.saveUserDetails(username: username, password: password)
                UserDefaults.standard.set(true, forKey: "dataflowSwitch")
                DispatchQueue.main.async { [weak self] in
                    self!.endSpinner()
                }
            }
            loginDelegate?.loginSubmitted(username: username, password: password)
        }
    }
    
    func startSpinner(){
        // add the spinner view controller
        addChild(spinnerController)
        spinnerController.view.frame = view.frame
        view.addSubview(spinnerController.view)
        spinnerController.didMove(toParent: self)
    }
    func endSpinner(){
        spinnerController.spinner.stopAnimating()
    }
    
    func clearFields(){
        usernameField.text = ""
        passwordField.text = ""
    }
    
    // TODO:
    func verifyUsername(username: String, password: String) -> Bool{
        // add code for verifying username and password
        let verified: Bool = true
        return verified
    }
    
    func saveUserDetails(username: String, password: String){
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.set(password, forKey: "password")
    }
    
}



class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}


// another test...
extension UIButton {
    func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        setBackgroundImage(image(withColor: color), for: state)
    }
}
private extension NSMutableAttributedString {
    func setSubstringAsLink(substring: String, linkURL: String) {
        let range = mutableString.range(of: substring)
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.link, value: linkURL, range: range)
        }
    }
}
