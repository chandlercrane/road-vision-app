//
//  CalampInfoViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/30/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- CalampInfoViewController.swift -- Chandler Crane -- 8/8/19
 
 This file is the screen that is shown after the info button is pressed from the table view of the Calamp VC. It just provides information about the bluetooth device that was found and allows for the ability to set and unset the given bluetooth device as the connected calamp device.
 */


import Foundation
import UIKit

final class CalampInfoViewController: UIViewController{
    
    var deviceNameText: String = ""
    var deviceUUIDText: String = ""
    var deviceStatText: String = ""
    
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceUUID: UILabel!
    @IBOutlet weak var deviceStat: UILabel!
    @IBOutlet weak var setCalampButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deviceName?.text = deviceNameText
        deviceUUID?.text = deviceUUIDText
        deviceStat?.text = deviceStatText
        setCalampButton.layer.cornerRadius = 22
        if deviceName.text == calampName && deviceUUID.text ==  calampUUID {
            setCalampButton.setTitle("Unset as Calamp Device", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func setCalampButtonPressed(_ sender: UIButton) {
        if(setCalampButton.titleLabel?.text == "Unset as Calamp Device"){
            calampUUID = ""
            calampName = ""
            calampStat = ""
            setCalampButton.setTitle("Set as Calamp Device", for: .normal)
        }
        else{
            calampUUID = deviceUUIDText
            calampName = deviceNameText
            calampStat = deviceStatText
            setCalampButton.setTitle("Unset as Calamp Device", for: .normal)
        }
    }
    

}

