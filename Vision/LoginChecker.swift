//
//  LoginChecker.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/31/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- LoginChecker.swift -- Chandler Crane -- 8/8/19
 
 This file checks to see if someone has *ever* logged into the app, if someone has, the app will load to the main Vision view, otherwise it will load to the initial login screen. It also contains the logic to save when a user has first logged into the app. Obviously, you can remove the code within the "submit" function so that a user is always directed to the login screen.
 */


import Foundation
import UIKit

private let loginCheckKey = "loginCheckKey"

struct LoginController {
    private init() {}
    
    static func checkSubmission() -> Bool {
        return UserDefaults.standard.bool(forKey: loginCheckKey)
    }
    
    static func submit(username: String, password: String) {
        UserDefaults.standard.setValue(true, forKey: loginCheckKey)
    }
}
