//
//  SettingsViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/22/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- SettingsViewController.swift -- Chandler Crane -- 8/8/19
 
 This file is the view for the settings screen. Nothing is out of the ordinary with this file. Just a few buttons to provide different functionality like login change, bluetooth calamp search, and back to vision view.
 */


import Foundation
import UIKit

final class SettingsViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var dataflowSwitch: UISwitch!
    @IBOutlet weak var changeUsernameButton: UIButton!
    @IBOutlet weak var userProfileName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userProfileName.text = UserDefaults.standard.string(forKey: "username") ?? ""
        dataflowSwitch.isOn = UserDefaults.standard.bool(forKey: "dataflowSwitch")
        changeUsernameButton.layer.cornerRadius =  22
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userProfileName.text = UserDefaults.standard.string(forKey: "username") ?? ""
        dataflowSwitch.isOn = UserDefaults.standard.bool(forKey: "dataflowSwitch")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeUsername(_ sender: UIButton) {
    }
    @IBAction func dataflowStatusChanged(_ sender: UISwitch) {
        UserDefaults.standard.set(dataflowSwitch.isOn, forKey: "dataflowSwitch")
    }
}
