//
//  BluetoothCheckViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 7/22/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- BluetoothCheckViewController.swift -- Chandler Crane -- 8/8/19
 
 This file is not currently connected but contains logic for what a view would look and how it would act for a stretch goal which was to check for bluetooth sensors on technician's tools to ensure on engine start that they have all their tools with them. This view would allow a technician to confirm he has everything if they wanted to, mark things as present/not present if it was marked incorrectly, and give a notification on start-up if not all tools were there.
 */


import Foundation
import UIKit

class BluetoothDeviceCell: UITableViewCell {
    @IBOutlet weak var bluetoothDeviceStatus: UIImageView!
    @IBOutlet weak var bluetoothDeviceName: UILabel!
    @IBOutlet weak var bluetoothDeviceStatusButton: UIButton!
    
    @IBAction func bluetoothDeviceStatusChange(_ sender: Any) {
        if(self.bluetoothDeviceStatusButton.titleLabel!.text == "Mark as Present"){
            self.bluetoothDeviceStatusButton.setTitle("Mark as Not Present", for: .normal)
            self.bluetoothDeviceStatus.image = UIImage(named: "positive")

        }
        else{
            self.bluetoothDeviceStatusButton.setTitle("Mark as Present", for: .normal)
            self.bluetoothDeviceStatus.image = UIImage(named: "negative")
        }
    }
    
}

final class BluetoothCheckViewController: UITableViewController {
    override func viewDidLoad() {
        self.clearsSelectionOnViewWillAppear = false
    }
    
 
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBAction func closeBluetoothDevices(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfBTDevices.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BluetoothDeviceCell", for: indexPath) as! BluetoothDeviceCell
        
        cell.bluetoothDeviceName?.text = listOfBTDevices[indexPath.row].name
        
        if(listOfBTDevices[indexPath.row].status == true){
            cell.bluetoothDeviceStatus.image = UIImage(named: "positive")
            cell.bluetoothDeviceStatusButton.setTitle("Mark as Not Present", for: .normal)
        }
        else{
            cell.bluetoothDeviceStatus.image = UIImage(named: "negative")
            cell.bluetoothDeviceStatusButton.setTitle("Mark as Present", for: .normal)
        }
 
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
