//
//  ChangeLoginViewController.swift
//  Vision
//
//  Created by CHANDLER CRANE on 8/2/19.
//  Copyright © 2019 AT&T. All rights reserved.
//

/*
 File Notes -- ChangeLoginViewController.swift -- Chandler Crane -- 8/8/19
 
 This file is similar to the other Login VC with the exception that it is not tied in with the AppDelegate, though it performs the exact same. The same "verifyUsername" function is used from the other Login VC as an object is instantiated and used to call the method.
 */


import Foundation
import UIKit
import MapboxVision


final class ChangeLoginViewController: UIViewController {
    
    weak var visionViewDelegate: VisionViewDelegate?
    

    let spinnerController = SpinnerViewController()
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var closeLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearFields()
        let tap = UITapGestureRecognizer(target: self.view, action: Selector("endEditing:"))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        signInButton.layer.cornerRadius =  22
    }
    
    func startSpinner(){
        
        // add the spinner view controller
        addChild(spinnerController)
        spinnerController.view.frame = view.frame
        view.addSubview(spinnerController.view)
        spinnerController.didMove(toParent: self)
    }
    func endSpinner(){
        spinnerController.spinner.stopAnimating()
    }
    
    func clearFields(){
        usernameText.text = ""
        passwordText.text = ""
    }
    
    @IBAction func closeLoginClicked(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signInClicked(_ sender: UIButton) {
        let username:String = usernameText.text!
        let password:String = passwordText.text!
        let l = LoginViewController()
        let verified: Bool = l.verifyUsername(username: username, password: password)
        startSpinner()
        if(verified){
            DispatchQueue.global(qos: .default).async {
                self.saveUserDetails(username: username, password: password)
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                DispatchQueue.main.async { [weak self] in
                    self!.endSpinner()
                }
            }
        }
    }
    
    
    func saveUserDetails(username: String, password: String){
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.set(password, forKey: "password")
    }
    
    private func launchVisionView(){
        
    }
}
